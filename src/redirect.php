<?php

function getRedirect($requestUri)
{
    if (strpos($requestUri, "/ssm") === 0) {
        $relativeUrl = substr($requestUri, 4);
        return "https://gitlab.ethz.ch/thealternative/courses/raw/master/scientific_software_management" . $relativeUrl;
    }

    return false;
}
