<?php

function sendMail($email, $message)
{
    // parse config
    list($username, $passwordAndHost, $port) = explode(":", $_ENV["MAILER_URL"]);
    list($password, $host) = explode("@", $passwordAndHost);

    // Create the Transport
    $transport = (new Swift_SmtpTransport($host, $port))
        ->setUsername($username)
        ->setPassword($password)
        ->setAuthMode("login")
        ->setEncryption("ssl");

    // Create the Mailer using your created Transport
    $mailer = new Swift_Mailer($transport);

    // Create a message
    $message = (new Swift_Message('thealternative.ch: Contact Request'))
        ->setFrom(['server@thealternative.ch'])
        ->setTo(['alexander.schoch@vseth.ethz.ch'])
        ->addReplyTo($email)
        ->addCC($email)
        ->setBody("email: $email\nmessage: $message");

    // Send the message
    $mailer->send($message);
}
