<?php

use Symfony\Component\Yaml\Yaml;

function getFutureEventFiles(): array
{
    $eventFiles = [];

    $today = new \DateTime("today");
    $date = $today->format("Y-m-d");

    foreach (glob(__DIR__ . "/../events/*.yml") as $eventFile) {
        $fileName = basename($eventFile);
        if ($fileName > $date) {
            $eventFiles[] = $eventFile;
        }
    }
    return $eventFiles;
}

function getPastEventFiles(): array
{
    $eventFiles = [];

    $today = new \DateTime("today");
    $date = $today->format("Y-m-d");

    $files = array_merge(glob(__DIR__ . "/../events/**/*.yml"), glob(__DIR__ . "/../events/*.yml"));
    foreach ($files as $eventFile) {
        $fileName = basename($eventFile);
        if ($fileName < $date) {
            $eventFiles[] = $eventFile;
        }
    }

    return $eventFiles;
}

function getEvent(string $filePath)
{
    $fileContent = file_get_contents($filePath);
    $event = Yaml::parse($fileContent);

    $fileName = basename($filePath);
    $date = substr($fileName, 0, 10);
    $name = substr($fileName, 11, -4);

    $event["name"] = $name;

    // format date for easy printing
    $dateObj = new \DateTime($date);
    $event["date"] = $dateObj->format("D d.m.Y") . " " . $event["startTime"] . " - " . $event["endTime"];

    // check if event is in the past and remove signup link if so
    $today = new \DateTime("today");
    if ($dateObj < $today) {
        $event["passed"] = true;
        if (isset($event["signup"])) {
            unset($event["signup"]);
        }

        // hide online participation URLs when the event is over
        if (filter_var($event["location"], FILTER_VALIDATE_URL) && strpos($event["location"], "https://ethz.zoom") === 0) {
            $event["location"] = "Online";
        }
    }

    return $event;
}

function printEventsList(array $relevantEventFiles, bool $areFutureEvents = false)
{
    // restrict list of future of events to 5, then show a "show more" link
    $maxEventsShown = $areFutureEvents ? 5 : 200;

    foreach ($relevantEventFiles as $relevantEventFile) {
        $event = getEvent($relevantEventFile);

        ?>
        <div class="event<?= --$maxEventsShown < 0 ? ' d-none' : '' ?><?= $event['name'] == 'Stammtisch' ? ' stammtisch' : '' ?>">
            <?php
            /** hide badges, as all events are going to happen (although online)
            <?php if (isset($event["cancelled"]) && $event["cancelled"]) { ?>
                <span class="badge badge-danger">cancelled</span>
            <?php } ?>
            <?php if (isset($event["tentative"]) && $event["tentative"]) { ?>
                <span class="badge badge-warning">tentative</span>
            <?php } ?>
            <?php if (isset($event["online"]) && $event["online"]) { ?>
                <span class="badge badge-success">online</span>
            <?php } ?>
            <?php if (isset($event["confirmed"]) && $event["confirmed"]) { ?>
                <span class="badge badge-success">confirmed</span>
            <?php } ?>
            **/ ?>
            <h2><?= $event["name"] ?></h2>
            <p class="subtitle">
                <?= $event["date"] ?> |
                <?php if (filter_var($event["location"], FILTER_VALIDATE_URL)) { ?>
                    <a href="<?= $event["location"] ?>" target="_blank"><?= $event["location"] ?></a>
                <?php } else { ?>
                    <?= $event["location"] ?>
                    <?php if (isset($event["certificate"])) { ?>
                        <small>with certificate</small>
                    <?php } ?>
                <?php } ?>
                <?php if (isset($event["signup"])) { ?>
                    | <a target="_blank" href="<?= $event["signup"] ?>">register</a>
                <?php } ?>
                <?php if (isset($event["signups"])) { ?>
                    | 
                    <?php foreach ($event["signups"] as $signup) { ?>
                        <a target="_blank" href="<?= $signup["link"] ?>"><?= $signup["label"] ?></a>
                    <?php } ?>
                <?php } ?>
                <?php if (isset($event["recording"])) { ?>
                    | <a target="_blank" href="<?= $event["recording"] ?>">recording</a>
                <?php } ?>
            </p>
            <div class="description">
                <?php if (isset($event["cancelled"]["reason"])) { ?>
                    <p class="alert alert-danger"><?= $event["cancelled"]["reason"] ?></p>
                <?php } ?>
                <?php if (isset($event["tentative"]["reason"])) { ?>
                    <p class="alert alert-warning"><?= $event["tentative"]["reason"] ?></p>
                <?php } ?>
                <?php if (isset($event["confirmed"]["reason"])) { ?>
                    <p class="alert alert-success"><?= $event["confirmed"]["reason"] ?></p>
                <?php } ?>
                <p>
                    <?= nl2br($event["description"]) ?>
                    <?php
                    if (isset($event["files"])) { ?>
                        <br/>
                        <span class="links">
                        <?php foreach ($event["files"] as $file) {
                            $path = $file["path"];
                            $target = filter_var($path, FILTER_VALIDATE_URL) ? $path : "/files/" . $path;
                            ?>
                            <span class="link">
                                <a target="_blank" href="<?= $target ?>"><?= $file["name"] ?></a>
                            </span>
                        <?php } ?>
                    </span>
                    <?php } ?>
                </p>

                <?php
                if (isset($event["sponsor"])) { ?>
                    <p class="mb-2"><em>Generously supported by</em></p>
                    <a target="_blank" href="<?= $event["sponsor"]["href"] ?>">
                        <img class="img-fluid sponsor-image"
                                src="images/sponsors/<?= $event["sponsor"]["image_src"] ?>"
                                alt="<?= substr($event["sponsor_image"]["image_src"], 0, -4) ?>">
                    </a>
                <?php } ?>
            </div>
        </div>
    <?php
    }

    if ($areFutureEvents) { ?>
        <div class="history">
            <div class="row">
                <div class="col">
                    <?php if ($maxEventsShown < 0) { ?>
                        <p>
                            <a id="view-all-events" href="#">
                                view all upcoming events
                            </a>
                        </p>
                    <?php } ?>
                </div>
                <div class="col">
                    <p class="text-right">
                        <a href="/past">view past events</a>
                    </p>
                </div>
            </div>
        </div>
    <?php
    }
}
