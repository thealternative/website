<?php

ini_set('display_errors', 'on');

include __DIR__ . "/../vendor/autoload.php";
include __DIR__ . "/config.php";
include __DIR__ . "/events.php";
include __DIR__ . "/sendmail.php";
include __DIR__ . "/redirect.php";

