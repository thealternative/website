<?php

use om\IcalParser;
use Symfony\Component\Yaml\Yaml;

include __DIR__ . "/src/bootstrap.php";

function cleanTitle(string $summary)
{
    // remove LinuxDays:
    if (strpos($summary, "LinuxDays: ") === 0) {
        $summary = substr($summary, strlen("LinuxDays: "));
    }

    // normalize stammtisch name
    if (strpos(strtolower($summary), "stammtisch") !== false) {
        $summary = "Stammtisch";
    }

    return $summary;
}

function cleanLocation(string $location)
{
    $coordinatePosition = strpos($location, " @47.");
    if ($coordinatePosition !== false) {
        $location = substr($location, 0, $coordinatePosition);
    }

    return $location;
}

function getIcsEvents($icalFile)
{
    $cal = new IcalParser();
    $cal->parseFile($icalFile);

    return $cal->getSortedEvents();
}

function parseIcsEvents(array $icanEvents)
{
    $result = [];
    $timeZone = new \DateTimeZone('Europe/Zurich');

    foreach ($icanEvents as $icsEvent) {
        $event = [];
        $event["startTime"] = $icsEvent['DTSTART']->setTimezone($timeZone)->format("H:i");
        $event["endTime"] = $icsEvent['DTEND']->setTimezone($timeZone)->format("H:i");
        $event["location"] = cleanLocation($icsEvent['LOCATION']);
        $event["description"] = $icsEvent['DESCRIPTION'];

        $date = $icsEvent['DTSTART']->format('Y-m-d');
        $title = cleanTitle($icsEvent['SUMMARY']);
        $eventName = $date . " " . $title;

        $result[$eventName] = $event;
    }

    return $result;
}

function getExistingEventPathLookup($eventDir)
{
    $eventPathLookup = [];

    $files = array_merge(glob($eventDir . "/*.yml"), glob($eventDir . "/**/*.yml"));
    foreach ($files as $eventPath) {
        $eventName = substr(basename($eventPath), 0, -4); // event name is filename minus extension
        $eventPathLookup[$eventName] = $eventPath;
    }

    return $eventPathLookup;
}

function calculateChangeSet(array $newEvents, array $existingEventPathLookup)
{
    $toAdd = [];
    $toUpdate = [];
    $toRemove = $existingEventPathLookup;

    foreach ($newEvents as $eventName => $event) {
        if (isset($existingEventPathLookup[$eventName])) {
            // hence do not remove
            unset($toRemove[$eventName]);

            // get event & remove view-only properties
            $existingEvent = getEvent($existingEventPathLookup[$eventName]);
            unset($existingEvent["date"]);
            unset($existingEvent["passed"]);

            // check for changes
            if ($existingEvent["startTime"] != $event["startTime"] ||
                $existingEvent["endTime"] != $event["endTime"] ||
                $existingEvent["location"] != $event["location"]
            ) {
                // do not overwrite description
                unset($event["description"]);

                $toUpdate[$eventName] = array_merge($existingEvent, $event);
            }
        } else {
            $toAdd[$eventName] = $event;
        }
    }

    return [$toAdd, $toUpdate, $toRemove];
}


$icsFile = "https://calendar.google.com/calendar/ical/b2ilmjr98gpcu38u4kk4qeucqs%40group.calendar.google.com/public/basic.ics";
$icsEvents = getIcsEvents($icsFile);
$newEvents = parseIcsEvents($icsEvents);

$eventDir = __DIR__ . "/events";
$existingEventPathLookup = getExistingEventPathLookup($eventDir);

list($toAdd, $toUpdate, $toRemove) = calculateChangeSet($newEvents, $existingEventPathLookup);

$changesCount = count($toAdd) + count($toUpdate) + count($toRemove);
if ($changesCount === 0) {
    echo "no changes detected.\n";
    exit;
} else {
    echo "detected " . $changesCount . " changes. \n";
}

if (count($toAdd) > 0) {
    echo "the following events will be added: \n";
    echo implode("\n", array_keys($toAdd));
    echo "\n";
}

if (count($toUpdate) > 0) {
    echo "the following events will be updated: \n";
    echo implode("\n", array_keys($toUpdate));
    echo "\n";
}

if (count($toRemove) > 0) {
    echo "the following events will be removed: \n";
    echo implode("\n", array_keys($toRemove));
    echo "\n";
}

$result = readline("Do you want to continue? [Y/n]: ");
if ($result !== "" && $result !== "Y") {
    exit;
}

$changes = array_merge($toAdd, $toUpdate);

foreach ($changes as $eventName => $event) {
    $filePath = isset($existingEventPathLookup[$eventName]) ? $existingEventPathLookup[$eventName] : $eventDir . "/" . $eventName . ".yml";

    $content = Yaml::dump($event, 2, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK + Yaml::DUMP_OBJECT_AS_MAP);

    file_put_contents($filePath, $content);
}

foreach ($toRemove as $path) {
    unlink($path);
}

echo "done!\n";
