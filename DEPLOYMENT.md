# Deployment

Deployment is automatic. The Gitlab CI will update the repository on the server with SSH.
Private key is stored in the project settings (CI -> Variables).
