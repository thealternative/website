<?php
include "../../src/bootstrap.php";
?>
<!DOCTYPE html>
<html lang="en">
<?php
include "../../templates/head.html"
?>

<body data-spy="scroll" data-target="#toc">

<?php
include "../../templates/header.html"
?>

<section>

    <div class="container">
        <div class="row">
            <!-- sidebar, which will move to the top on a small screen -->
            <div class="col-sm-3">
                <nav id="toc" data-toggle="toc" class="sticky-top"></nav>
            </div>
            <!-- main content area -->
            <article class="col-sm-9">

            <?php
            include "./bashguide.html"
            ?>

            </article>
        </div>
    </div>
</section>
<?php
include "../../templates/scripts.html"
?>

</body>
</html>
