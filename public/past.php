<?php
include "../src/bootstrap.php";
?>
<!DOCTYPE html>
<html lang="en">
<?php
include "../templates/head.html"
?>
<body>

<?php
include "../templates/header.html"
?>

<section>
    <div class="container">
        <div class="page-header">
            <h1 class="mt-5">Past events</h1>
            <p>
                Download Slides & other resources used at those events, or simply revel in the past.
            </p>
        </div>
    </div>
</section>

<section id="events">
    <div class="container">
        <div class="events">
            <?php

            $eventFiles = getPastEventFiles();
            $reversedEventFiles = array_reverse($eventFiles);

            printEventsList($reversedEventFiles);

            ?>
        </div>
    </div>
</section>

<?php
include "../templates/scripts.html"
?>

</body>
</html>
