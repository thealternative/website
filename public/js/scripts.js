hljs.initHighlightingOnLoad();

$(document).ready(function () {
    initializeEvents();
    initializeSmoothScrolling();
    tryInitializeMaps();
    tryInitializeAdblockWarning();
});

function tryInitializeAdblockWarning() {
    if (window.adblocker_active !== false) {
        // ads.js was not loaded; hence very likely an ad blocker
        return;
    }

    $.getScript("https://pagead2.googlesyndication.com/pagead/show_ads.js")
        .done(function (script, textStatus) {
            $(".adblock-warning").removeClass("d-none");
        })
        .fail(function (jqxhr, settings, exception) {
            // all fine; we have adblock enabled
        });
}


function initializeEvents() {
    $(".event a").on("click", function (e) {
        e.stopPropagation();
    });

    $(".event").on("click", function () {
        if (window.getSelection().toString()) {
            return;
        }

        $(this).toggleClass("active");
    });

    $("#view-all-events").on("click", function () {
        $(this).addClass("d-none");
        $(".event").removeClass("d-none");
    });
}

function initializeSmoothScrolling() {
    $('[data-spy="scroll"]').each(function () {
        $(this).scrollspy('refresh');
    });

    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });
}

function tryInitializeMaps() {
    if (!$("#maps").length) {
        return
    }

    // initialize map
    const map = L.map('maps');
    map.scrollWheelZoom.disable();

    // coordinates of CAB E13
    const lat = 47.378532;
    const lon = 8.5488535;
    const zoomLevel = 17;
    map.setView([lat, lon], zoomLevel);

    // set map tiles source
    const layer = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
        maxZoom: 18
    });
    layer.addTo(map);

    // add marker to the map
    const marker = L.marker([lat, lon]);
    marker.addTo(map);

    const popup = marker.bindPopup("<b>The Alternative</b><br/>CAB E14<br/>Universitätsstrasse 6<br/>8092 Zürich");
    popup.openPopup();
}
