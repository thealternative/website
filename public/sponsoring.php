<?php
include "../src/bootstrap.php";
?>
<!DOCTYPE html>
<html lang="en">
<?php
include "../templates/head.html"
?>
<body>

<?php
include "../templates/header.html"
?>

<section>
    <div class="container">
        <div class="page-header">
            <h1 class="mt-5">Sponsoring</h1>
            <p>Help us help our peers with Linux and Free Software / Open Source!</p>
        </div>

        <div class="page-content">
            <h2>About us</h2>
            <p>
                We organize for our peers the so-called "Linux Days", which consist of:
            </p>
            <ul>
                <li>lectures to understand the vision & ethics behind FOSS.</li>
                <li>install events to setup a distribution of their choice on their own device.</li>
                <li>workshops to get the most out of linux & other tools.</li>
                <li>"spotlight" lectures focusing on the more specialized, technical side of a specific technology such
                    as Docker or Virtualization.
                </li>
            </ul>
            <p>
                Further, we provide free tech support, have a monthly Stammtisch discussing FOSS related stuff, and
                occasionally organize talks. In May 2019, we had the honour to host Richard Stallman, which notably
                shaped the Free Software movement and
                started the work on GNU and GCC.
            </p>
            <p>
                About a third of our members study computer science, but we have many members from other departments
                such as
                Math, Physics, Chemistry, ...
                Usually our members join in their bachelors, and stay until they finish their studies.
            </p>

            <div class="mt-5"></div>
            <p>
                <b>Help us help our peers to get to know Linux and Free Software / Open Source!</b>
            </p>

            <p>
                <b>Your core benefits:</b>
            </p>
            <ul>
                <li><b>Reach a technical audience interested in Linux</b></li>
                <li><b>Help spread the word about Linux & FOSS</b></li>
                <li><b>Make your company known at ETH</b></li>
            </ul>

            <div class="plan">
                <h2>Install Events Sponsor</h2>
                <p><small>CHF 800 </small></p>
                <p>
                    You support our most important events, our install events:
                    We help students to install distributions such as Fedora & Ubuntu on their own devices,
                    giving them a kick start
                    into the Linux / FOSS world.
                    Our instructors (around 30 people) ensure that around 120 installations go smoothly and
                    without data loss.
                </p>

                <p>
                    Your contribution allows us:
                </p>
                <ul>
                    <li>to provide snacks & refreshments for the participants and the instructors.</li>
                    <li>to buy adapters & memory sticks to allow installations on any device.</li>
                </ul>

                <p>
                    You benefit from:
                </p>
                <ul>
                    <li>your logo & a short message projected into the room during the event (over a period totaling at
                        least 6 hours).
                    </li>
                    <li>your flyers / stickers layed out prominently next to the snacks.</li>
                </ul>
            </div>

            <div class="plan">
                <h2>"Spotlight" Sponsor</h2>
                <p><small>CHF 1000</small></p>
                <p>
                    You hold a technical talk connected to Linux and FOSS as part of the Linux Days.
                </p>

                <p>
                    Your contribution allows us:
                </p>
                <ul>
                    <li>to provide an interesting technical talk to our peers.</li>
                    <li>to make the Linux Days possible as a whole.</li>
                </ul>

                <p>
                    You benefit from:
                </p>
                <ul>
                    <li>our support & publicity channels while preparing the talk to reach the audience you aim
                        for.
                    </li>
                    <li>sharing the way your company solves technical problems with potential employees.</li>
                </ul>
            </div>

            <div class="plan">
                <h2>Linux Days Sponsor</h2>
                <p><small>CHF 2000</small></p>

                <p> You support the Linux Days as a whole and our activities during the semester.
                    By being the Linux Days Sponsor, you automatically also are the Install Events Sponsor
                    (including all benefits).
                </p>
                <p>
                    Your contribution allows us:
                </p>
                <ul>
                    <li>to organize the Install Events with snacks & proper equipment.</li>
                    <li>to provide interesting lectures and workshows to our peers.</li>
                    <li>to support our peers throughout the semester.</li>
                </ul>

                <p>
                    You benefit from:
                </p>
                <ul>
                    <li>all benefits of the Install Events Sponsor.</li>
                    <li>your logo on our flyers (distributed to all freshmen of ETH) and posters (hanging around
                        on campus).
                    </li>
                    <li>your logo on our webpage during the Linux Days (usually >4000 unique visitors).</li>
                </ul>
            </div>
            <div class="mt-5">
                <p>Reach us under sponsoring at thealternative.ch.</p>
            </div>
        </div>
    </div>
</section>

<?php
include "../templates/scripts.html"
?>

</body>
</html>
