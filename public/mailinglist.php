<?php
header("Location: https://lists.hostpoint.ch/mailman3/lists/events.thealternative.ch/");
die();

include "../src/bootstrap.php";

$error = FALSE;
$success = FALSE;

if (isset($_POST["email"])) {
    $url = 'https://admin.hostpoint.ch/mailman/subscribe/events_thealternative.ch';
    $data = array('email' => $_POST["email"], 'fullname' => $_POST["fullname"], 'pw' => "", 'pw-conf' => "");

    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = @file_get_contents($url, false, $context);
    if ($result === FALSE) {
        $error = TRUE;
    } else {
        $success = TRUE;
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<?php
include "../templates/head.html"
?>
<body>

<?php
include "../templates/header.html"
?>

<section>
    <div class="container">
        <div class="page-header">
            <h1 class="mt-5">Mailing List</h1>
        </div>
        <div class="page-content">
            <p>
                Stay up to date about all our upcoming events! We send you a preview at the beginning of each semester, and then a reminder one day before each event. There will be no spam, and you may leave the mailing list at anytime.
            </p>
            <form method="post" action="/mailinglist.php">
                <?php if ($success) { ?>
                    <div class="alert alert-success">
                        Great to see you joining our mailing list. Pleas check your mails to confirm you sign up!
                    </div>
                <?php } else if ($error) { ?>
                    <div class="alert alert-error">
                        Error signing up. Please contact us directly.
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label for="inputEmail">Email Address</label>
                    <input name="email" type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="Enter email" required>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="inputName">Your Name (optional)</label>
                    <input name="fullname" type="text" class="form-control" id="inputName" aria-describedby="nameHelp" placeholder="First Name">
                    <small id="nameHelp" class="form-text text-muted">We'll only use it to address you by your name.</small>
                </div>
                <button type="submit" name="email-button" class="btn btn-primary">Sign Up</button>
            </form>
        </div>
    </div>
</section>

<?php
include "../templates/scripts.html"
?>


</body>
</html>
