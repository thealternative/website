# website

the webpage of TheAlternative

## overview

`src` is the public folder, hence all requests land there.  
there is no framework used, which should make maintenance easier.

if you need to add a new page, use `sponsoring.php` as a template (as its the easiest to understand).

pages:
- `/` (`src/index.php`, special case) displays the homepage. the listed events are read out of `.yml` files in the `events` folder; only future events are shown.
- `/past` (`src/past.php`) shows all events from the `events` and subfolders; only past events are shown.
- `/sponsoring` (`src/sponsoring.php`) contains sponsoring information.
- `/guides/bash` (`src/guides/bash.php`) contains pandoc output from the bashcourse repository. `bash` syntax highlight & table of contents are preconfigured.

## update events

### automatically from google calender

execute `php update_events.php`.

the start/end time & the location of events will be updated, **but not the description**.  
after the event is created for the first time, update the description to suit the webpage.

this assumes the content of the calender is secure to be pasted into a webpage.  
else this is an easy way in for an cross-site scripting attack!

### manual

add new events in the `events` folder as `.yml` files. 
The filename must be of the form `2019-04-15 Introduction to Free Software.yml`.

File content example:

```yml
startTime: 17:15
endTime: 19:00 # optional
location: UZH KOL F 118, Rämistrasse 71, Zürich
description: >
  Discover the differences between Free and Open Source Software (FOSS) and proprietary software, and find out why these differences matter. 
  This captivating lecture will provide you with information about the advantages of Free Software licenses, as well as show you the historical background of the Linux kernel. 
  Additionally, we show why Free Software and Open Data is important even beyond the world of Linux.

signup: https://tim1.ethz.ch/cgi-bin/course/view.cgi?course_id=81 # optional, hidden for past events

files: # (optional)
 - name: slides (pdf)
   path: FS2019/FS19-FOSS.pdf  # (must be relative to public/files)
```
